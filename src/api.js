import axios from "axios";

const base = "http://127.0.0.1:8000/api";

export default {
  auth: {
    login: credentials =>
      axios.post(`${base}/token/`, credentials).then(res => res.data),
    signup: credentials => {
      console.log(credentials);
      return axios
        .post(`${base}/signup/`, { ...credentials })
        .then(response => response.data);
    }
  },
  house: {
    getAll: () => axios.get(`${base}/agency/housing/`).then(res => res.data),
    getOne: id =>
      axios.get(`${base}/agency/housing/${id}/`).then(res => res.data)
  },
  comment: {
    getAll: () => axios.get(`${base}/agency/comment/`).then(res => res.data),
    getOne: id =>
      axios.get(`${base}/agency/comment/${id}/`).then(res => res.data),
    send: (text, pk) =>
      axios.post(`${base}/agency/comment/`, { text, pk }).then(res => res.data)
  }
};
