import React from "react";
import ReactDOM from "react-dom";

import App from "./components/App/App";
import * as serviceWorker from "./utils/serviceWorker";
import { BrowserRouter } from "react-router-dom";
import { Route } from "react-router";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min";

const rootElement = document.getElementById("root");
ReactDOM.render(
  <BrowserRouter>
    <Route component={App} />
  </BrowserRouter>,
  rootElement
);

serviceWorker.unregister();
