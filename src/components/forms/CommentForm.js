import React, { Component } from "react";

class CommentForm extends Component {
  state = {
    data: {
      text: ""
    },
    errors: {},
    loading: false
  };

  onChange = e =>
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.props.onSubmit(this.state.data);
      this.setState({ data: { ...this.state.data, text: "" } });
    }
  };

  validate = data => {
    const errors = {};
    if (!data.text) errors.text = "This field is required!";
    return errors;
  };

  render() {
    const { errors } = this.state;
    return (
      <form method="post" onSubmit={this.onSubmit} className="py-4">
        <div className="form-group">
          <label htmlFor="text">Текст сообщения</label>
          <input
            onChange={this.onChange}
            type="text"
            name="text"
            className="form-control"
            value={this.state.text}
          />
          <span style={{ color: "red" }}>{errors.text}</span>
        </div>
        <button className="btn btn-primary">Отправить</button>
      </form>
    );
  }
}

export default CommentForm;
