import React from "react";

import "./App.sass";
import { Route } from "react-router";
import IndexPage from "../pages/IndexPage";
import HousePage from "../pages/HousePage";
import Navigation from "../layouts/Navigation";
import LoginPage from "../pages/LoginPage";
import RealtorPage from "../pages/RealtorPage";
import SignupPage from "../pages/SignupPage";
import setAuthHeaders from "../../utils/setAuthHeaders";
import api from "../../api";

class App extends React.Component {
  state = {
    authorized: false
  };

  componentWillMount() {
    if (localStorage.AgencyToken) {
      setAuthHeaders(localStorage.AgencyToken);
      this.setState({ authorized: true });
    } else {
      this.setState({ authorized: false });
    }
  }

  signUp = credentials => {
    console.log("lol");
    api.auth
      .signup(credentials)
      .then(res => this.login(credentials.username, credentials.password));
  };

  logout = () => {
    this.setState({ authorized: false });
    setAuthHeaders();
    localStorage.removeItem("AgencyToken");
  };

  login = (login, password) =>
    api.auth
      .login({ username: login, password })
      .then(res => {
        localStorage.AgencyToken = res.access;
        setAuthHeaders(res.access);
        this.setState({ authorized: true, errors: "" });
        this.props.history.push("/");
      })
      .catch(err =>
        this.setState({ errors: "Авторизационные данные не верны" })
      );

  render() {
    const { location } = this.props;
    const { authorized } = this.state;
    return (
      <div className="App">
        <div className="container">
          <Navigation authorized={authorized} logout={this.logout} />

          <Route path="/" exact component={IndexPage} />
          <Route
            path="/house/:id"
            render={({ match }) => (
              <HousePage authorized={authorized} match={match} />
            )}
          />
          <Route
            location={location}
            path="/realtor/:username"
            render={({match}) => <RealtorPage authorized={authorized} match={match} />}
          />
          <Route
            location={location}
            path="/login"
            render={() => <LoginPage login={this.login} />}
          />
          <Route
            location={location}
            path="/signup"
            render={() => <SignupPage signup={this.signUp} />}
          />
        </div>
      </div>
    );
  }
}

export default App;
