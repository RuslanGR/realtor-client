import React from "react";
import { NavLink, Link } from "react-router-dom";

const Navigation = ({ authorized, logout }) => (
  <nav className="navbar navbar-expand-lg navbar-light">
    <div className="container">
      <Link className="navbar-brand text-primary" to="/">
        <i className="fa d-inline fa-lg fa-circle-o" />
        <b>Logo</b>
      </Link>
      <button
        className="navbar-toggler navbar-toggler-right border-0"
        type="button"
        data-toggle="collapse"
        data-target="#navbar5"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbar5">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <NavLink
              activeClassName="my-active-link"
              className="nav-link"
              to="/about"
            >
              Index
            </NavLink>
          </li>
          {authorized && (
            <li className="nav-item">
              <NavLink
                activeClassName="my-active-link"
                className="nav-link"
                to="/dashboard"
              >
                Панель администратора
              </NavLink>
            </li>
          )}
        </ul>
        <ul className="navbar-nav float-right">
          {authorized ? (
            <li className="nav-item">
              <Link
                onClick={() => logout()}
                activeClassName="my-active-link"
                className="nav-link"
                to="/"
              >
                Выйти
              </Link>
            </li>
          ) : (
            <>
              <li className="nav-item">
                <NavLink
                  activeClassName="my-active-link"
                  className="nav-link"
                  to="/login"
                >
                  Войти
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  activeClassName="my-active-link"
                  className="nav-link"
                  to="/signup"
                >
                  Зарегистрироваться
                </NavLink>
              </li>
            </>
          )}
        </ul>
      </div>
    </div>
  </nav>
);

export default Navigation;
