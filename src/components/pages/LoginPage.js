import React, { Component } from "react";

class LoginPage extends Component {
  state = {
    login: "",
    password: "",
  };

  submit = event => {
    event.preventDefault();
    const { login, password } = this.state;
    this.props.login(login, password);
  };

  onChange = e => this.setState({ [e.target.name]: e.target.value });

  render() {
    const { login, password } = this.state;

    return (
      <div className="container py-4">
        <div className="row">
          <div className="col-md-4 offset-md-4">
            {this.props.errors && (
              <h6 style={{ color: "red" }}>{this.props.errors}</h6>
            )}
            <form method="POST" onSubmit={this.submit}>
              <div className="form-group">
                <label htmlFor="">Логин</label>
                <input
                  type="text"
                  className="form-control"
                  value={login}
                  onChange={this.onChange}
                  name="login"
                />
              </div>
              <div className="form-group">
                <label htmlFor="">Пароль</label>
                <input
                  type="password"
                  className="form-control"
                  value={password}
                  onChange={this.onChange}
                  name="password"
                />
              </div>
              <button className="btn btn-primary">Войти</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginPage;