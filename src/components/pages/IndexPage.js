import React, { Component } from "react";
import api from "../../api";
import { Link } from "react-router-dom";

const Card = ({ house: { title, description, id, image } }) => (
  <div>
    <Link to={`/house/${id}`}>
      <img src={image} width="200px" alt="" />
      <h4>{title}</h4>
    </Link>
    <p>{description}</p>
  </div>
);

class IndexPage extends Component {
  state = {
    houses: []
  };
  componentDidMount() {
    api.house.getAll().then(res => this.setState({ houses: res }));
  }

  render() {
    const { houses } = this.state;
    return (
      <div className="container">
        <h2>Houses</h2>
        {houses && houses.map(house => <Card key={house.id} house={house} />)}
      </div>
    );
  }
}

export default IndexPage;
