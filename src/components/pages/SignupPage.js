import React from "react";
import { Redirect } from "react-router-dom";

import SignupForm from "../forms/SignupForm";

const SignupPage = ({ isAuthenticated, signup }) =>
  !isAuthenticated ? (
    <div className="container">
      <div className="col-4 offset-4">
        <SignupForm onSubmit={signup} />
      </div>
    </div>
  ) : (
    <Redirect to="/" />
  );

export default SignupPage;
