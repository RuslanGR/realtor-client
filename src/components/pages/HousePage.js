import React, { Component } from "react";
import api from "../../api";
import { Link } from "react-router-dom";
import CommentForm from "../forms/CommentForm";

class HousePage extends Component {
  state = {
    house: null,
    comments: []
  };

  componentDidMount() {
    const { id } = this.props.match.params;
    api.house.getOne(id).then(res => this.setState({ house: res }));
    api.comment.getOne(id).then(res => this.setState({ comments: res }));
  }

  submit = ({ text }) => {
    api.comment.send(text, this.props.match.params.id);
    this.forceUpdate();
  };

  render() {
    const { house, comments } = this.state;
    const { authorized } = this.props;
    return (
      <div className="container">
        <h3>House</h3>
        {house && (
          <>
            <p>{house.title}</p>
            <img src={house.image} width="400px" alt="house" />

            <p>
              Риэлтор {house.user.username}
              <Link to={`/realtor/${house.user.username}`}>Написать</Link>
            </p>
          </>
        )}
        {comments && (
          <ul>
            {comments.map(comment => (
              <li key={comment.id}>
                {comment.user.username} {comment.text}
              </li>
            ))}
          </ul>
        )}

        {authorized ? (
          <CommentForm onSubmit={this.submit} />
        ) : (
          <p>Авторизуйтесь для возможности прокоментировать дом</p>
        )}
      </div>
    );
  }
}

export default HousePage;
